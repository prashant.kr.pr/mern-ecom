import axios from 'axios';
import getAuthHeader from './getAuthHeader';

export const getCurrentUser = async () => {
    try {
        const { data } = await axios.post(
            `${process.env.REACT_APP_API_URL}/v1/auth`,
            {},
            { headers: await getAuthHeader() }
        )
        return data;
    } catch (e) {
        console.error(e);
    }
}

export const login = async (email) => {
    try {
        await axios.post(
            `${process.env.REACT_APP_API_URL}/v1/login`,
            { email }
        );
        return { success: true }
    } catch (error) {
        switch (error.response.status) {
            case 400:
                return { error: 'Bad Login Data. Did you submit a valid login id.' }
            default:
                return { error: 'There was a error loging you in. Please try latter.' }
        }
    }
}