import axios from 'axios';
import Product from '../models/Product';

export const getProducts = async (categories) => {
    try{
        const { data } = await axios.get(
            `${process.env.REACT_APP_API_URL}/v1/products${categories ? `?categories=${categories}` : ''}`
        );
        return data.map(product => new Product(product));
    } catch(e) {
        console.error(e);
    }
}

export const getProduct = async (id) => {
    try{
        const { data } = await axios.get(
            `${process.env.REACT_APP_API_URL}/v1/products/${id}`
        );
        return new Product(data);
    } catch(e) {
        console.error(e);
    }
}