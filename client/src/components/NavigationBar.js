import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom';
import './NavigationBar.css';

export default class NavigationBar extends Component {
    render() {

        const { isLoggedIn, itemsInCart } = this.props;

        return (
            <div className="NavigationBar">
                <Link to="/">Home</Link>
                <Link to="/category/sporty">Sports</Link>
                <Link to="/category/tshirts">T-Shirt</Link>
                <Link to="/category/jackets">Jacket</Link>
                <Link to="/category/sneakers">Sneakers</Link>
                <Link to="/category/boots">Boots</Link>
                {isLoggedIn ?
                 <Fragment>
                  <Link to="/cart">My Cart({itemsInCart})</Link>
                  <Link to="/orders">Orders</Link>
                 </Fragment> :
                 <Link to="/account">Account</Link>
                }
            </div>
        )
    }
}
