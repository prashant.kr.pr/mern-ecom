import React, { Component } from 'react';
import './ProductCart.css';
import { SecondaryButton } from '../Button';

export default class ProductCart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            image: this.props.images[0]
        }
    }
    
    handleMouseHover = () => {
        if(this.props.images.length > 1) {
            this.setState({
                image:this.props.images[1]
            })
        }
        
    }
    handleMouseLeave = () => {
        this.setState({
            image:this.props.images[0]
        })
    }

    render() {
        return (
            <div 
                className="ProductCard"
                style={
                    this.props.pull ? 
                    {alignSelf: 'flex-end'} : 
                    null
                }
            >
                <img 
                    src={this.state.image}
                    onMouseOver={this.handleMouseHover}
                    onMouseLeave={this.handleMouseLeave}
                    alt="alt-text"
                />
                <h3>{this.props.name}</h3>
                <p>{this.props.price}</p>
                {this.props.withRemoveButton && 
                <SecondaryButton onClick={this.props.onRemove}>
                    Remove
                </SecondaryButton>}
            </div>
        )
    }
}
