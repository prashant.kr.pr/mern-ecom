import React, { Component } from 'react'

export default class DynamicCounter extends Component {

    render() {
        return (
            <div>
                <button onClick={() => this.props.increment(this.props.label)}>+1</button>
                <span>{this.props.label}: {this.props.value}</span>
                <button onClick={() => this.props.decrement(this.props.label)}>-1</button>
            </div>
        )
    }
}
