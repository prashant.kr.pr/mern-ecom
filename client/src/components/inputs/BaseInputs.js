import React, { Component } from 'react';
import './BaseInput.css';

class BaseInputs extends Component {
    render() {
        return (
            <div className={
                `BaseInput ${this.props.type === 'checkbox' ? 'BaseInputReverse' : ''}`
            }>
                <label htmlFor={this.props.name} data-testid="baseInputLabel">
                    {this.props.label}
                </label>
                <input
                    id={this.props.name}
                    {...this.props}
                />
            </div>
        );
    }
}

export default BaseInputs;