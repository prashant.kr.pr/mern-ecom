import React, { Component } from 'react';
import BaseInputs from './BaseInputs';

class TextInput extends Component {
    render() {
        return (
            <BaseInputs
                {...this.props}
                type="text" 
            />
        );
    }
}

export default TextInput;