export default class User {
    /**
     * 
     * @param {String} id
     * @param {String} username
     * @param {String} email
     * @param {String} role 
     */
    constructor({ id, username, email, role }) {
        this._id = id;
        this._username = username;
        this._email = email;
        this._role = role;
    }

    /**
     * @return {String}
     */
    getId = () => this._id;

    /**
     * @return {String}
     */
    getUsername = () => this._username;

    /**
     * @return {String}
     */
    getEmail = () => this._email;

    /**
     * @return {String}
     */
    getRole = () => this._role;

    /**
     * @return {{id:String, username:String, email:String, role:String}}
     */
    getData = () => ({
        id: this._id,
        username: this._username,
        email: this._email,
        role: this._role
    });
}