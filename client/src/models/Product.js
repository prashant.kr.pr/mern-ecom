export default class Product {
    /**
     * 
     * @param {String} _id
     * @param {String} name
     * @param {number} price
     * @param {Array, <string>} images 
     */
    constructor({ _id, name, price, images }) {
        this._id = _id;
        this._name = name;
        this._price = price;
        this._images = images;
    }

    /**
     * @returns {String}
     */
    getId = () => this._id;

    /**
     * @returns {String}
     */
    getName = () => this._name;

    /**
     * @returns {number}
     */
    getPrice = () => this._price;

    /**
     * @returns {number}
     */
    getFormattedPrice = () => 
        `${String(this._price / 100)}`;
    

    /**
     * @returns {Array<string>}
     */
    getImages = () => this._images;

    /**
     * @returns {{_id: String, name:String, price:number, formattedPrice:number, images:Array<String>}}
     */
    getData = () => ({
        _id: this._id,
        name: this._name,
        price: this._price,
        formattedPrice: this.getFormattedPrice(),
        images: this._images,
    })
}
