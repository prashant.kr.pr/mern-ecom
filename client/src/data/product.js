const images1 = [
    'https://images.unsplash.com/photo-1563927646365-02e40b3a5361?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60',
    'https://images.unsplash.com/photo-1563903227677-988f00ecc0fd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60'
];

const images2 = [
    'https://images.unsplash.com/photo-1556910146-6121b9613959?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60',
    'https://images.unsplash.com/photo-1563974514898-7aea295e12fa?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60'
];

const images3 = [
    'https://images.unsplash.com/photo-1563974698479-c18295caae0c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60'
];

export default [{
    id:1,
    name:'prod1',
    images:images1,
    price:100
},
{
    id:2,
    name:'prod2',
    images:images2,
    price:200
},
{
    id:3,
    name:'prod3',
    images:images3,
    price:300
}]