import mongoose from 'mongoose';
require('dotenv').config();

mongoose.connect(process.env.DB_PATH_END_POINT);
const db = mongoose.connection;

db.on('error', (error) => {
    console.error(error);
})

db.once('open', () => {
    console.log('connection successfull.');
})

export default db;