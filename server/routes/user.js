import { UserModel } from '../models/User';

export default (app) => {
    app.get('/v1/users', async (req, res) => {
        if(!req.isAdmin) {
            return res.status(403).end();
        }
        var userData = await UserModel.find() || [];
        res.send(userData);
    })

    app.get('/v1/users/:id', async (req, res) => {
        try {
            var user = await UserModel.findById(req.params.id);
            if (user) {
                res.send(user);
            } else {
                res.status(404).end();
            }
        } catch (e) {
            res.status(404).end();
        }
    })

    app.post('/v1/users', (req, res) => {
        console.log((req.body));
        res.status(200).end();
    })
    app.put('/v1/users/:id', (req, res) => {
        console.log((req.params.id));
        res.status(200).end();
    })
    app.delete('/v1/users/:id', (req, res) => {
        console.log((req.params.id));
        res.status(200).end();
    });
}