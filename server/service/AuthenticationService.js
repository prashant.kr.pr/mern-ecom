const jwt = require('jsonwebtoken');
require('dotenv').config();

class AuthenticationService {
  generate = async (user) => {
    return await jwt.sign({
      exp: Math.floor(Date.now() / 1000) + (7 * 24 * 60 * 60),
      data: user,
    }, 'MERNStackCourse');
  }
}

const AuthenticationServiceSingleton = new AuthenticationService();

export default AuthenticationServiceSingleton;
