import express from 'express';
require('dotenv').config();
import bodyParser from 'body-parser';
import cors from 'cors';

import users from './mocks/Users';
import withAdminPermission from './middleware/withAdminPermission';
import logger from './middleware/logger';
import withAuthentication from './middleware/withAuthentication';
import db from './db/index';
import getUserRoutes from './routes/user';
import getProductRoutes from './routes/product';
import getAuthRoutes from './routes/auth';
import getOrderRoutes from './routes/orders';

const PORT = process.env.PORT;
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(withAuthentication);
app.use(withAdminPermission);
app.use(logger);
getUserRoutes(app);
getProductRoutes(app);
getAuthRoutes(app);
getOrderRoutes(app);

app.get('/', (req, res) => {
    res.send('Hello World');
})

app.get('/heartbeat', (req, res) => {
    res.send('Hello Heartbeat');
})

app.listen(PORT, () => {
    console.log(`Server started at ${PORT} `);
})