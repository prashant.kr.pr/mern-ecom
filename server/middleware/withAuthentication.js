import jwt from 'express-jwt';

export default jwt({
  secret: 'MERNStackCourse',
  credentialsRequired: false,
  getToken: (req) => {
    if (
      req.headers &&
      req.headers.authorization &&
      req.headers.authorization.split(' ')[0] === 'Bearer'
    ) {
      return req.headers.authorization.split(' ')[1]; // token
    }
    return null;
  },
});
